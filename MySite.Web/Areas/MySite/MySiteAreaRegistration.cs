using System.Web.Mvc;

namespace MySite.Web.Areas.mytenant
{
    public class mytenantAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "mytenant";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Register your MVC routes in here
        }
    }
}
