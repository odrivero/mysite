<configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
<sitecore>
    <contentSearch>
        <indexConfigurations>
            <!-- SOLR Sitecore compatibility table https://kb.sitecore.net/articles/227897 -->
            <!-- Please read more on how to setup SOLR on a Sitecore intance here: https://doc.sitecore.net/sitecore_experience_platform/82/setting_up_and_maintaining/search_and_indexing/walkthrough_setting_up_solr -->
            <MySiteSolrIndexConfiguration ref="contentSearch/indexConfigurations/scoreSolrConfiguration">
                <!-- For Solr Initialize() needs to be called after the IOC container has fired up -->
                <initializeOnAdd>false</initializeOnAdd>

                <!-- DEFAULT FIELD MAPPING
                     The field map allows you to have full control over the way your data is stored in the index. This affects the way data is queried,
                     the performance of searching, and the way that data is retrieved and cast to a proper type in the API.
                -->
                <fieldMap ref="contentSearch/indexConfigurations/scoreSolrConfiguration/fieldMap">
                </fieldMap>

                <documentOptions ref="contentSearch/indexConfigurations/scoreSolrConfiguration/documentOptions">
                    <!-- GLOBALLY INCLUDE TEMPLATES IN INDEX
                         This setting allows you to only include items that are based on specific templates in the index. You must specify all the
                         templates that you want to include, because template inheritance is not checked. 
                         When you enable this setting, all the items that are based on other templates are excluded, regardless of whether the template
                         is specified in the ExcludeTemplate list or not.
                    -->
                    <!--
                    <include hint="list:AddIncludedTemplate">
                        <BucketFolderTemplateId>{ADB6CA4F-03EF-4F47-B9AC-9CE2BA53FF97}</BucketFolderTemplateId>
                    </include>
                    -->

                    <!-- COMPUTED INDEX FIELDS 
                         This setting allows you to add fields to the index that contain values that are computed for the item that is being indexed.
                         You can specify the storageType and indextype for each computed index field in the <fieldMap><fieldNames> section.
                    -->
                    <fields hint="raw:AddComputedIndexField">
                        <field fieldName="default" tileName="default" siteContext="MySite">Score.Custom.Indexing.PreviewField, Score.Custom</field>
                        <field fieldName="search result" tileName="Search Result" siteContext="MySite">Score.Custom.Indexing.PreviewField, Score.Custom</field>
                        <field fieldName="autocomplete" tileName="Autocomplete" siteContext="MySite">Score.Custom.Indexing.PreviewField, Score.Custom</field>
                        <field fieldName="quick view" tileName="Quick View" siteContext="MySite">Score.Custom.Indexing.PreviewField, Score.Custom</field>
                    </fields>
                </documentOptions>

                <!-- **** Inherited default configuration **** -->
                <fieldReaders ref="contentSearch/indexConfigurations/scoreSolrConfiguration/fieldReaders"/>
                <indexFieldStorageValueFormatter ref="contentSearch/indexConfigurations/scoreSolrConfiguration/indexFieldStorageValueFormatter"/>
                <indexDocumentPropertyMapper ref="contentSearch/indexConfigurations/scoreSolrConfiguration/indexDocumentPropertyMapper"/>
                <documentBuilderType ref="contentSearch/indexConfigurations/scoreSolrConfiguration/documentBuilderType"/>
            </MySiteSolrIndexConfiguration>
        </indexConfigurations>

    </contentSearch>
</sitecore>
</configuration>
