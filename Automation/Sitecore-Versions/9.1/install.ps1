#Requires -Version 5.1
#Requires -RunAsAdministrator
#Requires -Modules SitecoreInstallFramework
<#
    .SYNOPSIS

    The installation script can be used to setup Sitecore 9.1 to a sandbox solution by using SIF 2.1.0.

    .EXAMPLE

    .\Automaton\Stecore-Versions\9.1\1_install.ps1 -site "mysite" -sqlServer "(local)\MSSQL2017" -sqlAdminUser "sa" -sqlAdminPassword "q1w2e3r4"

    -site "mysite" - the url of the sandbox site running locally WITHOUT THE http:// on it

    -sqlServer "(local)\MSSQL2017" - MSSQL instance 

    -sqlAdminUser "sa" - mssql user

    -sqlAdminPassword "q1w2e3r4" - connection string


    .EXAMPLE

    .\Automation\Sitecore-Versions\9.1\install.ps1 -site "mysite" -solrUrl "https://localhost:8983/solr" -solrRoot "C:\solr-7.2.1\" -solrService "SOLR 7.2.1" -sqlServer "(local)\MSSQL2017" -sqlAdminUser "sa" -sqlAdminPassword "q1w2e3r4"

    -xp0Package "Sitecore 9.1.0 rev. 001564 (WDP XP0 packages).zip"

    -site "mysite" - the url of the sandbox site running locally WITHOUT THE http:// on it

    -solrUrl "https://localhost:8983/solr" - the url of the SOLR instnace with HTTPS

    -solrRoot "C:\solr-7.2.1\" - an absolute path to SOLR folder

    -solrService "SOLR 7.2.1" - SOLR service name registered on your local.

    -sqlServer "(local)\MSSQL2017" - MSSQL instance 

    -sqlAdminUser "sa" - mssql user

    -sqlAdminPassword "q1w2e3r4" - connection string
    
    -InstallDirectory ".\sandbox\" - a relative path to the current folder

    -License ".\license.xml" - a license xml file for Sitecore

#>
Param (
    [Parameter(Mandatory=$False)]
    [string]$xp0Package,

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$site,

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SolrUrl = "https://localhost:8983/solr",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SolrRoot = "C:\solr\",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SolrService = "SOLR 7.2.1",

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$SqlServer, # "(local)\MSSQL2016" 

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$SqlAdminUser, # = "sa" 

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$SqlAdminPassword, # = "q1w2e3r4" 

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SitecoreAdminPassword = "b",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$InstallDirectory = ".\sandbox\",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$License = ".\license.xml",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [switch]$skipPrereq, # skip prerequirements check

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [switch]$uninstall #add -uninstall to uninstall Sitecore 9.1 instance
)

$XConnectCollectionService = "$site.xconnect" 
$IdentityServerSiteName = "$site.identityserver"
$sitecoreSiteName = "$site"

if(!(Test-Path $License)) {
    Write-Host "Could not load find a license file by path: '$License' " -ForegroundColor Red
    exit 1;
}

if([string]::IsNullOrEmpty($xp0Package)){
    write-host "Looking for a Sitecore XP0 package...";
    $packages = Get-ChildItem -Filter "*(WDP XP0 packages).zip";
    if($packages.Count -gt 1) {
        Write-Host "Please specify what Sitecore package you want to install. We found more than one:" -ForegroundColor Red;
        $packages | % { Write-Host $_.Name -ForegroundColor Red };
        Exit 1;
    }

    if($packages.Length -le 0) {
        Write-Host "Please specify what Sitecore package you want to install. Or copy Sitecore XP0 package to '$(Resolve-Path ".")' folder." -ForegroundColor Red;
        Exit 1;
    }

    $xp0Package = $packages[0].FullName;
    Write-host "Found '$($xp0Package | split-path -leaf)' package" -ForegroundColor Green; 
} else {
    Write-Host "Check the package: '$xp0Package'":
    if(!(Test-Path $xp0Package)) {
        Write-Host "Cannot find a package by path: $xp0Package" -ForegroundColor red;
        exit 1;
    }

    $xp0Package = Resolve-Path $xp0Package;
    Write-Host "Sitecore XP0 package OK" -ForegroundColor Green;
} 

if(!(Test-Path $InstallDirectory)) {
    $InstallDirectory = (New-Item -path $InstallDirectory -ItemType Directory).FullName;
} else {
    $InstallDirectory = Resolve-Path $InstallDirectory;
}

$scriptsFolder = Join-Path $InstallDirectory "scripts";
Write-Host "Unzip Sitecore XP0 package to scripts folder: $scriptsFolder";

try {
    Expand-Archive $xp0Package $scriptsFolder -Force;
}
catch {
    Write-Host "Could not unzip archive $xp0Package" -ForegroundColor Red;
    Exit 1;
}
Write-Host "Sitecore XP0 unziped.";

Write-Host "Looking for XP0 configurations";
$configsArchive = (get-childitem -path $scriptsFolder -filter "XP0 configuration files*").FullName;
if([string]::IsNullOrEmpty($configsArchive)) {
    Write-Host "Was not able to find a XP0 configurations archive. Installation aborted." -ForegroundColor Red;
    exit 1;
}
try {
    Expand-Archive $configsArchive $scriptsFolder -Force;
}catch {
    Write-Host "Could not unzip XP0 configuration archive '$configsArchive'";
    exit 1;
}
Write-Host "'$($configsArchive | split-path -leaf)' configurations unzipped" -ForegroundColor Green;


Write-Host "Looking for newtonsoft.json.dll...";
if(!(Test-Path (Join-Path $scriptsFolder "Newtonsoft.json.dll"))) {
    $archiveWithJson = (Get-ChildItem $scriptsFolder -Filter "*xp0xconnect.scwdp.zip").FullName;
    Write-Host "Going to look for Newtonsoft.json.dll in '$($archiveWithJson | split-path -leaf)' archive.";

    Add-Type -Assembly System.IO.Compression.FileSystem

    $zip = [IO.Compression.ZipFile]::OpenRead($archiveWithJson)
    $entries = $zip.Entries | ? { $_.Name -like 'Newtonsoft.json.dll' } | select -first 1;

    if($entries.count -gt 0) {
        Write-Host "Extracting '$($entries.FullName)'...";
    }

    $entries | % { [IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$scriptsFolder\$($_.Name)"); }

    $zip.Dispose()
}

Write-Host "About to load Newtonsoft.json.dll from '$scriptsFolder' folder..."
[void][System.Reflection.Assembly]::LoadFile(("$scriptsFolder\Newtonsoft.Json.dll"));
Write-Host "Newtonsoft.json.dll successfulyl loaded." -ForegroundColor Green;

Write-Host "Looking for XP0 installation configurations..."
$IdentityConfigPath =  "$scriptsFolder\IdentityServer.json";
if(!(Test-Path $IdentityConfigPath)) {
    Write-Host "Could not find a configuration file for Identity Server Installation. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}

$XConnConfigPath =  "$scriptsFolder\xconnect-xp0.json";
if(!(Test-Path $XConnConfigPath)) {
    Write-Host "Could not find a configuration file for XConnect Installation. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}

$ScConfigPath =  "$scriptsFolder\sitecore-XP0.json";
if(!(Test-Path $ScConfigPath)) {
    Write-Host "Could not find a configuration file for Sitecore Installation. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}

$SingleDevPath =  "$scriptsFolder\XP0-SingleDeveloper.json";
if(!(Test-Path $SingleDevPath)) {
    Write-Host "Could not find a configuration file for a Single Developer Installation environment. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}

$PrerequisitesPath =  "$scriptsFolder\Prerequisites.json";
if(!(Test-Path $PrerequisitesPath)) {
    Write-Host "Could not find a configuration file for Prerequisites Installation. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}


write-host "Found configurations $($IdentityConfigPath | split-path -leaf), $($XConnConfigPath | split-path -leaf), $($ScConfigPath | split-path -leaf), $($SingleDevPath | split-path -leaf)" -ForegroundColor Green;


Write-Host "Looking for XP0 installation archives..."
$SitecorePackage = (get-childitem $scriptsFolder -Filter "*single.scwdp.zip").FullName
if([string]::IsNullOrEmpty($SitecorePackage)) {
    Write-Host "Looking for Sitecore XP0 installation archive. Please make sure you have 'Sitecore 9.x.x rev. xxxxxx (OnPrem)_single.scwdp.zip' in '$scriptsFolder' folder." -ForegroundColor Red
    exit 1;
} else {
    Write-Host "Found '$SitecorePackage' installation archive" -ForegroundColor Green;
}

$XConnectPackage = (get-childitem $scriptsFolder -Filter "*xconnect.scwdp.zip").FullName;
if([string]::IsNullOrEmpty($XConnectPackage)) {
    Write-Host "Looking for XConnect XP0 installation archive. Please make sure you have 'Sitecore 9.x.x rev. xxxxxx (OnPrem)_xp0xconnect.scwdp.zip' in '$scriptsFolder' folder." -ForegroundColor Red
    exit 1;
} else {
    Write-Host "Found '$XConnectPackage' installation archive" -ForegroundColor Green;
}

$IdentityServerPackage = (get-childitem $scriptsFolder -Filter "*identityserver.scwdp.zip").FullName;
if([string]::IsNullOrEmpty($IdentityServerPackage)) {
    Write-Host "Looking for Identity Server installation archive. Please make sure you have 'Sitecore.IdentityServer x.x.x rev. xxxxx (OnPrem)_identityserver.scwdp.zip' in '$scriptsFolder' folder." -ForegroundColor Red
    exit 1;
} else {
    Write-Host "Found '$IdentityServerPackage' installation archive" -ForegroundColor Green;
}

function Add-InstallDirectory {
    param(
        [Parameter(Mandatory=$True)]
        [string]$filePath,

        [Parameter(Mandatory=$True)]
        [string]$outFile
    )

    $config = [Newtonsoft.Json.JsonConvert]::DeserializeObject((Get-Content $filePath | out-string));
    if($config) {
        if(!($config.Parameters["InstallDirectory"])) {
            $InstallDirectory = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"DefaultValue`":`"C:\\inetpub\\wwwroot\\`", `"Description`": `"The install diectory for the site.`"}");
            $config.Parameters.Add("InstallDirectory", $InstallDirectory);
        }
        if(!([string]::IsNullOrEmpty($config.Variables["Site.PhysicalPath"].ToString()))) {
            $config.Variables["Site.PhysicalPath"] = New-Object Newtonsoft.Json.Linq.JValue -ArgumentList "[joinpath(parameter('InstallDirectory'), parameter('SiteName'))]";
        }

        [Newtonsoft.Json.JsonConvert]::SerializeObject($config, [Newtonsoft.Json.Formatting]::Indented) | Out-File $outFile -Encoding ASCII;
    }
}

function Update-SingleDeveloperConfig {
    param(
        [Parameter(Mandatory=$True)]
        [string]$filePath,

        [Parameter(Mandatory=$True)]
        [string]$outFile
    )

    $config = [Newtonsoft.Json.JsonConvert]::DeserializeObject((Get-Content $filePath | out-string));
    if($config) {
        if(!($config.Parameters["InstallDirectory"])) {
            $InstallDirectory = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"DefaultValue`":`"C:\\inetpub\\wwwroot\\`", `"Description`": `"The install diectory for the site.`"}");
            $config.Parameters.Add("InstallDirectory", $InstallDirectory);
        }

        @(
            "SitecoreXP0"
            "XConnectXP0"
            "IdentityServer"
        ) | % {
            $sectionName = $_;
            if(!($config.Parameters["$($sectionName):InstallDirectory"])) {
                $sectionInstallDirectory = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"Reference`":`"InstallDirectory`", `"Description`": `"Override to pass InstallDirectory value to $($sectionName) config.`"}");
                $config.Parameters.Add("$($sectionName):InstallDirectory", $sectionInstallDirectory);
            }
        } 

        if(!($config.Parameters["IdentityServerCertificates:RootCertFileName"])) {
            $rootCertName = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"Description`": `"Override to pass RootCertFileName value to IdentityServerCertificates config.`", `"Reference`": `"IdentityServerRootCertFileName`"}");
            $config.Parameters.Add("IdentityServerCertificates:RootCertFileName", $rootCertName);
        }

        if(!($config.Parameters["IdentityServerRootCertFileName"])) {
            $rootCertName = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"Description`": `"The file name of the root certificate to be created.`", `"DefaultValue`": `"SitecoreRootCert`"}");
            $config.Parameters.Add("IdentityServerRootCertFileName", $rootCertName);
        }

        if(!($config.Parameters["XConnectCertificates:RootCertFileName"])) {
            $rootCertName = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"Description`": `"Override to pass RootCertFileName value to IdentityServerCertificates config.`", `"Reference`": `"XConnectRootCertFileName`"}");
            $config.Parameters.Add("XConnectCertificates:RootCertFileName", $rootCertName);
        }

        if(!($config.Parameters["XConnectRootCertFileName"])) {
            $rootCertName = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"Description`": `"The file name of the root certificate to be created.`", `"DefaultValue`": `"SitecoreRootCert`"}");
            $config.Parameters.Add("XConnectRootCertFileName", $rootCertName);
        }


        [Newtonsoft.Json.JsonConvert]::SerializeObject($config, [Newtonsoft.Json.Formatting]::Indented) | Out-File $outFile -Encoding ASCII;
    }
}

Copy-Item $License $scriptsFolder -Force;

Write-Verbose "Modify $($IdentityConfigPath | split-path -leaf) with InstallDirectory";
Add-InstallDirectory $IdentityConfigPath $IdentityConfigPath;

Write-Verbose "Modify $($XConnConfigPath | split-path -leaf) with InstallDirectory";
Add-InstallDirectory $XConnConfigPath $XConnConfigPath;

Write-Verbose "Modify $($ScConfigPath | split-path -leaf) with InstallDirectory";
Add-InstallDirectory $ScConfigPath $ScConfigPath;

Write-Verbose "Modify $($SingleDevPath | split-path -leaf) with overrides for InstallDirectory and RootCertificate"
Update-SingleDeveloperConfig $SingleDevPath $SingleDevPath;


# load a proper version of SitecoreInstallFramework
get-module SitecoreInstallFramework | remove-module;
try {
    Import-Module -FullyQualifiedName @{ModuleName = 'SitecoreInstallFramework'; RequiredVersion = '2.1.0'} -ErrorAction Stop;
}
catch [System.IO.FileNotFoundException] {
    Write-host "Was not able to find `"SitecoreInstallFramework`" module version 2.1.0. Going to install it..."
    Install-Module "SitecoreInstallFramework" -RequiredVersion "2.1.0";
    Import-Module -FullyQualifiedName @{ModuleName = 'SitecoreInstallFramework'; RequiredVersion = '2.1.0'} -ErrorAction Stop;
}

# run prerequirements if not an uninstall script.
if(!$skipPrereq) {
    Install-SitecoreConfiguration $PrerequisitesPath
}

# Install XP0 via combined partials file.
$singleDeveloperParams = @{
    Path = $SingleDevPath
	InstallDirectory = $InstallDirectory

    SqlServer = $SqlServer
    SqlAdminUser = $SqlAdminUser
    SqlAdminPassword = $SqlAdminPassword
    
    SolrUrl = $SolrUrl
    SolrRoot = $SolrRoot
    SolrService = $SolrService
    Prefix = $site

    XConnectRootCertFileName = "ROOT_$XConnectCollectionService"
    XConnectCertificateName = $XConnectCollectionService
    IdentityServerRootCertFileName = "ROOT_$IdentityServerSiteName"
    IdentityServerCertificateName = $IdentityServerSiteName

    XConnectPackage = $XConnectPackage
    SitecorePackage = $SitecorePackage
    IdentityServerPackage = $IdentityServerPackage

    IdentityServerSiteName = $IdentityServerSiteName
    XConnectSiteName = $XConnectCollectionService
    SitecoreSitename = $SitecoreSiteName
    SitecoreAdminPassword = $SitecoreAdminPassword

    PasswordRecoveryUrl = "http://$SitecoreSiteName"
    SitecoreIdentityAuthority = "https://$IdentityServerSiteName"
    XConnectCollectionService = "https://$XConnectCollectionService"

    ClientSecret = "SIF-Default"
    AllowedCorsOrigins = "http://$SitecoreSiteName"
    LicenseFile = "$scriptsFolder\license.xml"
}

try{
    # we have to push location, because SingleDev Configuration has relative paths for includes
    Push-Location $scriptsFolder

    if($uninstall) {
        Uninstall-SitecoreConfiguration @singleDeveloperParams -Verbose *>&1 | Tee-Object "$scriptsFolder\XP0-SingleDeveloper-Uninstall.log"
    } else {
        Install-SitecoreConfiguration @singleDeveloperParams -Verbose *>&1 | Tee-Object "$scriptsFolder\XP0-SingleDeveloper.log" 
    }
} finally {
    Pop-Location
}