#Requires -Version 5.1
#Requires -RunAsAdministrator
#Requires -Modules SitecoreInstallFramework, SitecoreFundamentals

<#
    .SYNOPSIS

    The installation script can be used to setup Sitecore to a sandbox solution by using SIF.

    .EXAMPLE

    .\Automaton\Stecore-Versions\9.0\1_install.ps1 -site "mysite" -sqlServer "(local)\MSSQL2016" -sqlAdminUser "sa" -sqlAdminPassword "q1w2e3r4"

    -site "mysite" - the url of the sandbox site running locally WITHOUT THE http:// on it

    -sqlServer "(local)\MSSQL2016" - MSSQL instance 

    -sqlAdminUser "sa" - mssql user

    -sqlAdminPassword "q1w2e3r4" - connection string


    .EXAMPLE

    .\Automaton\Stecore-Versions\9.0\1_install.ps1 -xp0Package "Sitecore 9.0.1 rev. 171219 (WDP XP0 packages).zip" -site "mysite" -solrUrl "https://localhost:8983/solr" -solrRoot "C:\solr\" -solrService "SOLR 6.6.1" -sqlServer "(local)\MSSQL2016" -sqlAdminUser "sa" -sqlAdminPassword "q1w2e3r4" -InstallDirectory ".\sandbox\" -License ".\license.xml"

    -xp0Package "Sitecore 9.0.1 rev. 171219 (WDP XP0 packages).zip"

    -site "mysite" - the url of the sandbox site running locally WITHOUT THE http:// on it

    -solrUrl "https://localhost:8983/solr" - the url of the SOLR instnace with HTTPS

    -solrRoot "C:\solr\" - an absolute path to SOLR folder

    -solrService "SOLR 6.6.1" - SOLR service name registered on your local.

    -sqlServer "(local)\MSSQL2016" - MSSQL instance 

    -sqlAdminUser "sa" - mssql user

    -sqlAdminPassword "q1w2e3r4" - connection string
    
    -InstallDirectory ".\sandbox\" - a relative path to the current folder

    -License ".\license.xml" - a license xml file for Sitecore

#>
Param (
    [Parameter(Mandatory=$False)]
    [string]$xp0Package,

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$site,

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SolrUrl = "https://localhost:8983/solr",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SolrRoot = "C:\solr\",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$SolrService = "SOLR 6.6.1",

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$SqlServer, # "(local)\MSSQL2016" 

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$SqlAdminUser, # = "sa" 

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$SqlAdminPassword, # = "q1w2e3r4" 

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$InstallDirectory = ".\sandbox\",

    [Parameter(Mandatory=$False)]
    [ValidateNotNullOrEmpty()]
    [string]$License = ".\license.xml"
)

$XConnectCollectionService = "$site.xconnect" 
$sitecoreSiteName = "$site"

if(!(Test-Path $License)) {
    Write-Host "Could not load find a license file by path: '$License' " -ForegroundColor Red
    exit 1;
}

if([string]::IsNullOrEmpty($xp0Package)){
    write-host "Looking for a Sitecore XP0 package...";
    $packages = Get-ChildItem -Filter "*(WDP XP0 packages).zip";
    if($packages.Count -gt 1) {
        Write-Host "Please specify what Sitecore package you want to install. We found more than one:" -ForegroundColor Red;
        $packages | % { Write-Host $_.Name -ForegroundColor Red };
        Exit 1;
    }

    if($packages.Length -le 0) {
        Write-Host "Please specify what Sitecore package you want to install. Or copy Sitecore XP0 package to '$(Resolve-Path ".")' folder." -ForegroundColor Red;
        Exit 1;
    }

    $xp0Package = $packages[0].FullName;
    Write-host "Found '$($xp0Package | split-path -leaf)' package" -ForegroundColor Green; 
} else {
    Write-Host "Check the package: '$xp0Package'":
    if(!(Test-Path $xp0Package)) {
        Write-Host "Cannot find a package by path: $xp0Package" -ForegroundColor red;
        exit 1;
    }

    $xp0Package = Resolve-Path $xp0Package;
    Write-Host "Sitecore XP0 package OK" -ForegroundColor Green;
} 

if(!(Test-Path $InstallDirectory)) {
    $InstallDirectory = (New-Item -path $InstallDirectory -ItemType Directory).FullName;
} else {
    $InstallDirectory = Resolve-Path $InstallDirectory;
}

$scriptsFolder = Join-Path $InstallDirectory "scripts";
Write-Host "Unzip Sitecore XP0 package to scripts folder: $scriptsFolder";

try {
    Expand-Archive $xp0Package $scriptsFolder -Force;
}
catch {
    Write-Host "Could not unzip archive $xp0Package" -ForegroundColor Red;
    Exit 1;
}
Write-Host "Sitecore XP0 unziped.";

Write-Host "Looking for XP0 configurations";
$configsArchive = (get-childitem -path $scriptsFolder -filter "XP0 configuration files*").FullName;
if([string]::IsNullOrEmpty($configsArchive)) {
    Write-Host "Was not able to find a XP0 configurations archive. Installation aborted." -ForegroundColor Red;
    exit 1;
}
try {
    Expand-Archive $configsArchive $scriptsFolder -Force;
}catch {
    Write-Host "Could not unzip XP0 configuration archive '$configsArchive'";
    exit 1;
}
Write-Host "'$($configsArchive | split-path -leaf)' configurations unzipped" -ForegroundColor Green;


Write-Host "Looking for newtonsoft.json.dll...";
if(!(Test-Path (Join-Path $scriptsFolder "Newtonsoft.json.dll"))) {
    $archiveWithJson = (Get-ChildItem $scriptsFolder -Filter "*xp0xconnect.scwdp.zip").FullName;
    Write-Host "Going to look for Newtonsoft.json.dll in '$($archiveWithJson | split-path -leaf)' archive.";

    Add-Type -Assembly System.IO.Compression.FileSystem

    $zip = [IO.Compression.ZipFile]::OpenRead($archiveWithJson)
    $entries = $zip.Entries | ? { $_.Name -like 'Newtonsoft.json.dll' } | select -first 1;

    if($entries.count -gt 0) {
        Write-Host "Extracting '$($entries.FullName)'...";
    }

    $entries | % { [IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$scriptsFolder\$($_.Name)"); }

    $zip.Dispose()
}

Write-Host "About to load Newtonsoft.json.dll from '$scriptsFolder' folder..."
[void][System.Reflection.Assembly]::LoadFile(("$scriptsFolder\Newtonsoft.Json.dll"));
Write-Host "Newtonsoft.json.dll successfulyl loaded." -ForegroundColor Green;

Write-Host "Looking for Sitecore XP0 installation archive..."
$SitecoreArchive = (get-childitem $scriptsFolder -Filter "*single.scwdp.zip").FullName
if([string]::IsNullOrEmpty($SitecoreArchive)) {
    Write-Host "Looking for Sitecore XP0 installation archive. Please make sure you have 'Sitecore 9.x.x rev. xxxxxx (OnPrem)_single.scwdp.zip' in '$scriptsFolder' folder." -ForegroundColor Red
    exit 1;
} else {
    Write-Host "Found '$SitecoreArchive' installation archive" -ForegroundColor Green;
}

Write-Host "Looking for XConnect XP0 installation archive..."
$XConnectArchive = (get-childitem $scriptsFolder -Filter "*xconnect.scwdp.zip").FullName;
if([string]::IsNullOrEmpty($XConnectArchive)) {
    Write-Host "Looking for XConnect XP0 installation archive. Please make sure you have 'Sitecore 9.x.x rev. xxxxxx (OnPrem)_xp0xconnect.scwdp.zip' in '$scriptsFolder' folder." -ForegroundColor Red
    exit 1;
} else {
    Write-Host "Found '$XConnectArchive' installation archive" -ForegroundColor Green;
}

$XConnConfigPath =  "$scriptsFolder\xconnect-xp0.json";
if(!(Test-Path $XConnConfigPath)) {
    Write-Host "Could not find a configuration file for XConnect Installation. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}

$ScConfigPath =  "$scriptsFolder\sitecore-XP0.json";
if(!(Test-Path $ScConfigPath)) {
    Write-Host "Could not find a configuration file for Sitecore Installation. Please make sure you have 'XP0 Configuration files 9.x.x rev. xxxxxx.zip' in '$xp0package' archive." -ForegroundColor Red
    exit 1;
}

function Add-InstallDirectory {
    param(
        [Parameter(Mandatory=$True)]
        [string]$filePath,

        [Parameter(Mandatory=$True)]
        [string]$outFile
    )

    $config = [Newtonsoft.Json.JsonConvert]::DeserializeObject((Get-Content $filePath | out-string));
    if($config) {
        if(!($config.Parameters["InstallDirectory"])) {
            $InstallDirectory = [Newtonsoft.Json.Linq.JObject]::Parse("{`"Type`": `"string`", `"DefaultValue`":`"C:\\inetpub\\wwwroot\\`", `"Description`": `"The install diectory for the site.`"}");
            $config.Parameters.Add("InstallDirectory", $InstallDirectory);
        }
        if(!([string]::IsNullOrEmpty($config.Variables["Site.PhysicalPath"].ToString()))) {
            $config.Variables["Site.PhysicalPath"] = New-Object Newtonsoft.Json.Linq.JValue -ArgumentList "[joinpath(parameter('InstallDirectory'), parameter('SiteName'))]";
        }

        [Newtonsoft.Json.JsonConvert]::SerializeObject($config) | Out-File $outFile;
    }
}

# load a proper version of SitecoreInstallFramework
get-module SitecoreInstallFramework | remove-module;
Import-Module -FullyQualifiedName @{ ModuleName = 'SitecoreInstallFramework'; RequiredVersion = '1.2.1' }

Copy-Item $License $scriptsFolder -Force

#install client certificate for xconnect 
$certParams = @{     
    Path = "$scriptsFolder\xconnect-createcert.json"     
    CertificateName = "$site.xconnect" 
    RootCertFileName = "ROOT_$site.xconnect"
    } 
    
Install-SitecoreConfiguration @certParams -Verbose 

 
#install solr cores for xdb 
$solrParams = 
@{     
    Path = "$scriptsFolder\xconnect-solr.json"     
    SolrUrl = $SolrUrl     
    SolrRoot = $SolrRoot     
    SolrService = $SolrService     
    CorePrefix = $site 
} 
Install-SitecoreConfiguration @solrParams -Verbose 

$modifiedXConnConfig = "$scriptsFolder\modified-$($XConnConfigPath | split-path -leaf)";
Add-InstallDirectory $XConnConfigPath $modifiedXConnConfig

#deploy xconnect instance 
$xconnectParams = @{     
    Path = $modifiedXConnConfig     
    Package = $XConnectArchive     
    LicenseFile = "$scriptsFolder\license.xml"     
    Sitename = $XConnectCollectionService     
    InstallDirectory = $InstallDirectory
    XConnectCert = $certParams.CertificateName     
    SqlDbPrefix = $site  
    SqlServer = $SqlServer  
    SqlAdminUser = $SqlAdminUser     
    SqlAdminPassword = $SqlAdminPassword     
    SolrCorePrefix = $site     
    SolrURL = $SolrUrl      
    } 

Install-SitecoreConfiguration @xconnectParams -Verbose 
 
#install solr cores for sitecore $solrParams = 
$solrParams = @{     
    Path = "$scriptsFolder\sitecore-solr.json"     
    SolrUrl = $SolrUrl     
    SolrRoot = $SolrRoot     
    SolrService = $SolrService     
    CorePrefix = $site 
} 

Install-SitecoreConfiguration @solrParams 
 
$modifiedScConfig = "$scriptsFolder\modified-$($ScConfigPath | split-path -leaf)";
Add-InstallDirectory $ScConfigPath $modifiedScConfig

#install sitecore instance 
$sitecoreParams = 
@{     
    Path = $modifiedScConfig     
    Package = $SitecoreArchive  
    LicenseFile = "$scriptsFolder\license.xml"     
    SqlDbPrefix = $site  
    SqlServer = $SqlServer  
    SqlAdminUser = $SqlAdminUser     
    SqlAdminPassword = $SqlAdminPassword     
    SolrCorePrefix = $site  
    SolrUrl = $SolrUrl     
    XConnectCert = $certParams.CertificateName     
    Sitename = $sitecoreSiteName         
    InstallDirectory = $InstallDirectory
    XConnectCollectionService = "https://$XConnectCollectionService"    
} 
Install-SitecoreConfiguration @sitecoreParams 